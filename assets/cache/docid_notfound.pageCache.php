<?php die('Unauthorized access.'); ?>a:37:{s:2:"id";s:1:"1";s:4:"type";s:8:"document";s:11:"contentType";s:9:"text/html";s:9:"pagetitle";s:19:"Купить чай";s:9:"longtitle";s:19:"Купить чай";s:11:"description";s:0:"";s:5:"alias";s:5:"index";s:15:"link_attributes";s:0:"";s:9:"published";s:1:"1";s:8:"pub_date";s:1:"0";s:10:"unpub_date";s:1:"0";s:6:"parent";s:1:"0";s:8:"isfolder";s:1:"0";s:9:"introtext";s:0:"";s:7:"content";s:2499:"<h1>Калмыцкий чай</h1>
<p>пресованной плиткой.<br />Состав: ветки чая<br />Вес плитки: 2,5 кг.<br />Произведено: в Грузии в 2018 году.<br />Срок годности: 2 года<br />Применение: для увеличения лактации, выведение радионуклеотидов из крови, повышение потенции и др.<br />При покупке одной плитки - цена: 799 руб<br />При покупке двух плиток - цена: 1199 руб<br />При покупке трех плиток - цена: 2099 руб<br />При покупке пяти плиток - цена: 3499 руб<br />=================================<br />Самовывоз:<br />м.Павелецкая с 10:00 до 18:00<br />м.Динамо с 19:00 до 23:30</p>
<p>Доставка:<br />по Москве 350 руб<br />по Москкой области от 800 руб<br />по России от 1000 руб<br />по СНГ от 2000 руб<br />=================================<br /><strong>Калмыцкий чай</strong> - крайне полезный напиток, его также рекомендуют диабетикам, молодым мамам для увеличения периода лактации, а также незаменим при интоксикации. Лично на себе опробовано. Были неоднократные случаи, когда решалась проблема с гастритом и язвой.</p>
<p>В нем имеются целебные танины, бодрящий кофеин, а также такие "хранители" здоровья и молодости, как катехины. Благодаря ингредиентам, которые добавляют к чаю во время его приготовления (масло, молоко и поваренная соль), он содержит в себе много фтора, калия и йода, а также натрия и марганца. Также этот напиток богат такими витаминами, как С, K, В и PP. Кроме того, в нем имеется и никотиновая кислота.</p>
<p>По желанию могу дать рекомендацию, как приготовить (несколько полезных рецептов)</p>";s:8:"richtext";s:1:"1";s:8:"template";s:1:"3";s:9:"menuindex";s:1:"0";s:10:"searchable";s:1:"1";s:9:"cacheable";s:1:"1";s:9:"createdby";s:1:"1";s:9:"createdon";s:10:"1130304721";s:8:"editedby";s:1:"1";s:8:"editedon";s:10:"1544176584";s:7:"deleted";s:1:"0";s:9:"deletedon";s:1:"0";s:9:"deletedby";s:1:"0";s:11:"publishedon";s:10:"1130304721";s:11:"publishedby";s:1:"1";s:9:"menutitle";s:14:"Главная";s:7:"donthit";s:1:"0";s:10:"privateweb";s:1:"0";s:10:"privatemgr";s:1:"0";s:13:"content_dispo";s:1:"0";s:8:"hidemenu";s:1:"0";s:13:"alias_visible";s:1:"1";s:17:"__MODxDocGroups__";s:0:"";}<!--__MODxCacheSpliter__--><!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimal-ui">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="imagetoolbar" content="no">
		<meta name="msthemecompatible" content="no">
		<meta name="cleartype" content="on">
		<meta name="HandheldFriendly" content="True">
		<meta name="format-detection" content="telephone=no">
		<meta name="format-detection" content="address=no">
		<meta name="google" value="notranslate">
		<meta name="theme-color" content="#ffffff">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
		<meta property="og:title" content="Купить чай | Купить чай"/>
		<meta property="og:description" content=""/>
		<meta property="og:image" content="/assets/templates/tea5/logo/tea5.jpg"/>
		<meta property="og:type" content="product"/>
		<meta property="og:url" content="http://tea5.ru/" />
		<meta property="og:site_name" content="Купить чай" />
		<meta name="application-name" content="Купить чай">
		<meta name="msapplication-tooltip" content="Купить чай">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/assets/templates/tea5/logo/tea5.jpg">
		<meta name="msapplication-starturl" content="http://tea5.ru/">
		<meta name="msapplication-tap-highlight" content="no">
		<meta name="msapplication-square70x70logo" content="/assets/templates/tea5/logo/tea5.jpg">
		<meta name="msapplication-square150x150logo" content="/assets/templates/tea5/logo/tea5.jpg">
		<meta name="msapplication-wide310x150logo" content="/assets/templates/tea5/logo/tea5.jpg">
		<meta name="msapplication-square310x310logo" content="/assets/templates/tea5/logo/tea5.jpg">
		
		<title>Купить чай</title>
		<meta name="description" content="">
		<meta name="keywords" content="">
		<!-- Google Tag Manager --><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-MX9K5LQ');</script><!-- End Google Tag Manager -->
	</head>
<body><!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MX9K5LQ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<h1>Калмыцкий чай</h1>
<p>пресованной плиткой.<br />Состав: ветки чая<br />Вес плитки: 2,5 кг.<br />Произведено: в Грузии в 2018 году.<br />Срок годности: 2 года<br />Применение: для увеличения лактации, выведение радионуклеотидов из крови, повышение потенции и др.<br />При покупке одной плитки - цена: 799 руб<br />При покупке двух плиток - цена: 1199 руб<br />При покупке трех плиток - цена: 2099 руб<br />При покупке пяти плиток - цена: 3499 руб<br />=================================<br />Самовывоз:<br />м.Павелецкая с 10:00 до 18:00<br />м.Динамо с 19:00 до 23:30</p>
<p>Доставка:<br />по Москве 350 руб<br />по Москкой области от 800 руб<br />по России от 1000 руб<br />по СНГ от 2000 руб<br />=================================<br /><strong>Калмыцкий чай</strong> - крайне полезный напиток, его также рекомендуют диабетикам, молодым мамам для увеличения периода лактации, а также незаменим при интоксикации. Лично на себе опробовано. Были неоднократные случаи, когда решалась проблема с гастритом и язвой.</p>
<p>В нем имеются целебные танины, бодрящий кофеин, а также такие "хранители" здоровья и молодости, как катехины. Благодаря ингредиентам, которые добавляют к чаю во время его приготовления (масло, молоко и поваренная соль), он содержит в себе много фтора, калия и йода, а также натрия и марганца. Также этот напиток богат такими витаминами, как С, K, В и PP. Кроме того, в нем имеется и никотиновая кислота.</p>
<p>По желанию могу дать рекомендацию, как приготовить (несколько полезных рецептов)</p>
</body>
{{footer}}