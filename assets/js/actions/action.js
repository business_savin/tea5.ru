/*
* http://example.com/?_ym_debug=1
* */
$(function() {

    /* modal orders */
    var modal_tpl = '<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">' +
        '<div class="modal-dialog">' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'+
        '<h4 class="modal-title" id="myModalLabel">Офорление заказа</h4></div>'+
        '<div class="modal-body"><h2 style="text-align: center">Загрузка...</h2></div>' +
        '<div class="modal-footer"><i>Нажав кнопку "Отправить" вы тем самым даёте своё согласие на обработку персональных данных. <a href="/agreements.html" target="_blank">Ознакомиться с соглашением <span class="glyphicon glyphicon-share"></span></a></i></div>' +
        '</div></div></div>';
    $(document).on('click touch', '.item-buy-button', function (event) {

        ym(51461786, 'reachGoal', 'item-buy-button'); //https://yandex.ru/support/metrika/general/goal-js-event.html#js-event

        if(!$(document).find('.modal')[0]){
            $('body').append(modal_tpl);
        }


        var kudadada = '/form-order/?v=1&id=10&name_tovar=' + $(this).find('img').attr('alt') + '&id_tovara=' + this.id; //form-order
        $.get(kudadada, function (resp) {
            $('.modal-body', document).html(resp);
        });

        $('.modal',document).modal();
        return false;
    });

    /* add to bookmark */
    function getBrowserInfo() {
        var t,v = undefined;
        if (window.chrome) t = 'Chrome';
        else if (window.opera) t = 'Opera';
        else if (document.all) {
            t = 'IE';
            var nv = navigator.appVersion;
            var s = nv.indexOf('MSIE')+5;
            v = nv.substring(s,s+1);
        }
        else if (navigator.appName) t = 'Netscape';
        return {type:t,version:v};
    }
    function bookmark(a){
        var url = window.document.location;
        var title = window.document.title;
        var b = getBrowserInfo();
        if (b.type == 'IE' && 8 >= b.version && b.version >= 4) window.external.AddFavorite(url,title);
        else if (b.type == 'Opera') {
            a.href = url;
            a.rel = "sidebar";
            a.title = url+','+title;
            return true;
        }
        else if (b.type == "Netscape") window.sidebar.addPanel(title,url,"");
        else alert("Нажмите CTRL-D, чтобы добавить страницу в закладки.");
        return false;
    }
    $(document).on('click touch', '.link__bookmark', function (event) {
            console.log('event bookmark')
            bookmark(event);
            return false;
        });

    /*
    * .form-order__btn-plus
    * .form-order__btn-minus
    * .form-order__count-info
    *
    * */


    $(document).on('click touch', '.form-order__btn-plus', function(){
        var input = $('.form-order__count-info', document);
        count = parseInt(input.val(), 10);
        count++;
        input.val(count);
    });

    $(document).on('click touch', '.form-order__btn-minus', function(){
        var input = $('.form-order__count-info', document);
        count = parseInt(input.val(), 10);
        count--;
        if(count <= 0){ count = 1; }
        input.val(count);
    });

});